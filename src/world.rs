// This file is part of Server Loader.
//
// Server Loader is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Server Loader is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Server Loader. If not, see <https://www.gnu.org/licenses/>.


use serde::{Serialize, Deserialize};
use std::path::{Path, PathBuf};
use tempfile::{NamedTempFile, TempDir};
use tar::Archive;
use flate2::read::GzDecoder;
use std::fs::{create_dir_all, read_dir, copy, rename, DirEntry};


use super::url::Url;
use crate::error::Error;


/// Represents a Server entry
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct World {
    name: String,
    url: String,
}


impl World {

    /// Fetch the world and put it in the folder
    /// It is expected that the world is stored in a tar.gz file
    pub fn load<T: Into<String> + AsRef<Path>>(&self, folder: T, oauth_token: &Option<String>) -> Result<(), Error> {
        let url = Url::new(self.url.clone(), oauth_token);

        let file = NamedTempFile::new()?;

        url.fetch_to_file(file.reopen()?)?;

        let folder = folder.into();
        let folder = Path::new(&folder);

        create_dir_all(folder)?;
        let decoder = GzDecoder::new(file);

        let temp_dir = TempDir::new()?;

        Archive::new(decoder).unpack(temp_dir.path())?;

        fn copy_dir(src: &Path, dest: &Path) -> Result<(), Error> {
            create_dir_all(dest)?;
            for entry in read_dir(src)? {
                let entry = entry?;
                let entry_type = entry.file_type()?;
                if entry_type.is_dir() {
                    copy_dir(&entry.path(), &dest.join(entry.file_name()))?;
                } else {
                    copy(&entry.path(), &dest.join(entry.file_name()))?;
                }
            }
            Ok(())
        }

        fn move_dir(src: &Path, dest: &Path) -> Result<(), Error> {
            match rename(src, dest) {
                Ok(()) => Ok(()),
                Err(_) => copy_dir(src, dest)
            }
        }

        fn is_dir(entry: Result<DirEntry, std::io::Error>) -> Option<PathBuf> {
            let entry = match entry {
                Ok(entry) => entry,
                Err(_) => return None
            };

            let file_type = match entry.file_type() {
                Ok(file_type) => file_type,
                Err(_) => return None,
            };

            if file_type.is_dir() {
                Some(entry.path())
            } else {
                None
            }
        }

        let entries = read_dir(temp_dir.path())?
            .filter_map(is_dir)
            .collect::<Vec<PathBuf>>();


        let name = Path::new(&self.name);

        if entries.len() == 1 {
            for e in entries {
                println!("{:?} -> {:?}/{:?}", e, folder, name, );
                move_dir(&e, &folder.join(name))?
            }
        } else {
            return Err(Error::MultipleWorld)
        }





        Ok(())
    }


    /// Get the world name
    pub fn get_name(&self) -> &str {
        &self.name[..]
    }
}


#[cfg(test)]
mod test {

    use super::World;

    use tempfile::TempDir;

    #[test]
    fn test_load() {
        let tmp_dir = TempDir::new().unwrap();

        World {name: "Vortex".to_string(),
                url: "https://gitlab.com/e4333/plugins/Vortex/-/archive/v0.1.1/Vortex-v0.1.1.tar.gz".to_string(),
        }.load(tmp_dir.path().to_str().unwrap(), &None).unwrap();

        // On different storage
        World {name: "Vortex".to_string(),
                url: "https://gitlab.com/e4333/plugins/Vortex/-/archive/v0.1.1/Vortex-v0.1.1.tar.gz".to_string(),
        }.load("test/tmp", &None).unwrap();
    }

    #[test]
    fn test_get_name() {
        assert_eq!(
            World {name: "Vortex".to_string(),
                url: "".to_string(),
            }.get_name(),
            "Vortex");
    }
}
