// This file is part of Server Loader.
//
// Server Loader is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Server Loader is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Server Loader. If not, see <https://www.gnu.org/licenses/>.


use std::fmt;


#[derive(Debug)]
pub enum Error {
    NoUrlNorRepo,
    MultipleWorld,
    IoError {
        error: std::io::Error,
    },
    CurlError {
        error: curl::Error,
    },
}

impl std::error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::NoUrlNorRepo => write!{f, "You have at least to define an url for the plugin or a repository for all the plugins"},
            Self::MultipleWorld => write!{f, "You need to have one world folder per entry"},
            Self::IoError {error} => error.fmt(f),
            Self::CurlError {error} => error.fmt(f),
        }
    }
}

impl From<std::io::Error> for Error {
    fn from(error: std::io::Error) -> Self {
        Self::IoError {error}
    }
}

impl From<curl::Error> for Error {
    fn from(error: curl::Error) -> Self {
        Self::CurlError {error}
    }
}


#[cfg(test)]
mod test {

    use std::io::ErrorKind;

    use super::Error;

    #[test]
    fn test_display() {
        assert_eq!(
            Error::NoUrlNorRepo.to_string(),
            "You have at least to define an url for the plugin or a repository for all the plugins");

        assert_eq!(
            Error::MultipleWorld.to_string(),
            "You need to have one world folder per entry");

        let err = curl::Error::new(0);
        assert_eq!(
            Into::<Error>::into(err).to_string(),
            "[0] No error");

        let err = std::io::Error::new(ErrorKind::Other, "Test Error");
        assert_eq!(
            Into::<Error>::into(err).to_string(),
            "Test Error");
    }

}
