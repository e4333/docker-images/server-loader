// This file is part of Server Loader.
//
// Server Loader is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Server Loader is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Server Loader. If not, see <https://www.gnu.org/licenses/>.


use clap::Parser;

use server_loader::config::Config;


#[derive(Parser, Debug)]
#[command(author, version, about)]
struct Args {
    /// Input file
    #[arg(default_value_t = String::from("server_loader.yml"))]
    input: String,

    /// Loads only the plugins
    /// When used with `--worlds-only`, loads nothing
    #[arg(short, long, default_value_t = false, verbatim_doc_comment)]
    plugins_only: bool,

    /// Loads only the worlds
    /// When used with `--plugins-only`, loads nothing
    #[arg(short, long, default_value_t = false, verbatim_doc_comment)]
    worlds_only: bool,

    /// Plugins folder
    #[arg(long, default_value_t = String::from("plugin"))]
    plugins_folder: String,

    /// Worlds folder
    #[arg(long, default_value_t = String::from("."))]
    worlds_folder: String,

    /// Token for authentification on sites like github and gitlab
    /// Will replace any `${oauth_token}` before retrieving the files
    #[arg(long, verbatim_doc_comment)]
    oauth_token: Option<String>,


}


fn main() {
    let args = Args::parse();

    let config = Config::new(args.input);

    if ! args.worlds_only {
        config.load_plugins(args.plugins_folder, &args.oauth_token)
    }

    if ! args.plugins_only {
        config.load_worlds(args.worlds_folder, &args.oauth_token)
    }
}
