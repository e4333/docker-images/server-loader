// This file is part of Server Loader.
//
// Server Loader is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Server Loader is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Server Loader. If not, see <https://www.gnu.org/licenses/>.


use serde::{Serialize, Deserialize};
use std::path::Path;

use std::fs::File;


use crate::error::Error;
use super::url::Url;


/// Represents a Plugin entry
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct Plugin {
    name: String,
    version: Option<String>,
    url: Option<String>,
}


impl Plugin {

    /// Fetch the plugin and put it in the folder
    pub fn load<T: Into<String> + AsRef<Path>>(&self, folder: T, repo: &Option<String>, oauth_token: &Option<String>) -> Result<(), Error>{

        let version = match self.version.clone() {
            Some(version) => version,
            None => "latest".to_string(),
        };

        let url = match self.url.clone() {

            Some(url) => url,

            None => match repo {
                None => return Err(Error::NoUrlNorRepo),

                Some(repo) => repo.to_owned() + "/" + &self.name + "/releases/download/v" + &version + "/" + &self.name + "-" + &version + ".jar",
            },
        };
        let url = Url::new(url, oauth_token);

        let folder: String = folder.into();
        let file = File::create(folder + "/" + &self.name + "-" + &version + ".jar")?;

        url.fetch_to_file(file)
    }

    /// Get the plugin name
    pub fn get_name(&self) -> &str {
        &self.name[..]
    }
}


#[cfg(test)]
mod test {

    use crate::error::Error;
    use super::Plugin;


    use tempfile::TempDir;

    #[test]
    fn test_load() {
        let tmp_dir = TempDir::new().unwrap();

        Plugin {name: "Vortex".to_string(),
                url: Some("https://gitlab.com/api/v4/projects/52073948/packages/generic/v0.1.0/plugin/Vortex.jar".to_string()),
                version: None
        }.load(tmp_dir.path().to_str().unwrap(), &None, &None).unwrap();

        Plugin {name: "Vortex".to_string(),
                url: Some("https://gitlab.com/api/v4/projects/52073948/packages/generic/v0.1.0/plugin/Vortex.jar".to_string()),
                version: Some("v0.1.0".to_string())
        }.load(tmp_dir.path().to_str().unwrap(), &None, &None).unwrap();

        Plugin {name: "AsyncWorldEdit".to_string(),
                url: None,
                version: Some("3.9.4".to_string()),
        }.load(tmp_dir.path().to_str().unwrap(), &Some("https://github.com/SBPrime/".to_string()), &None).unwrap();

        assert_eq!(
            Plugin {name: "Vortex".to_string(),
                url: None,
                version: None
            }.load("", &None, &None).map_err(|err| err.to_string()),
            Err(Error::NoUrlNorRepo.to_string()));
    }

    #[test]
    fn test_get_name() {
        assert_eq!(
            Plugin {name: "Vortex".to_string(),
                url: None,
                version: None
            }.get_name(),
            "Vortex");
    }
}
