// This file is part of Server Loader.
//
// Server Loader is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Server Loader is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Server Loader. If not, see <https://www.gnu.org/licenses/>.


use serde::{Serialize, Deserialize};
use std::path::Path;
use std::fs::{File, create_dir_all};
use std::io::ErrorKind;


use super::plugin::Plugin;
use super::world::World;


/// Represents the Config file used by Server Loader
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct Config {
    #[serde(alias="repository")]
    repo: Option<String>,
    plugins: Option<Vec<Plugin>>,
    worlds: Option<Vec<World>>,
}

impl Config {

    /// Creates a new instance from the given file
    ///
    /// # Example
    ///
    /// ```
    /// use server_loader::config::Config;
    ///
    /// Config::new("test/input.yml");
    /// ```
    ///
    ///
    /// # File Structure
    /// The file has to be a yaml file (other support may be added later)
    ///
    /// ## Keys
    /// The high level keys are to be ones of the following; none are mandatory
    /// - `plugins`
    /// - `worlds`
    /// - `repo` or `repository`
    ///
    /// ### Repository
    /// The repository key has to  be matched with a String.
    /// This String is used as a base url for the plugins when none are provided.
    /// It is used under the form `<repo>/<plugin>/releases/download/v<version>/<plugin>-<version>.jar`
    ///
    /// ### Plugins
    /// The plugins key has to be paired with a list of entry containings at least the plugin `name`.
    /// The optional entries are :
    /// - `url` which is used to fetch the plugin (it is prioritary over repo).
    /// - `version` the version, by default it is set to latest.
    /// A plugin will be interpreted as a jar file.
    ///
    /// ### Worlds
    /// The worlds key has to be paired with a list of entry containings the worlds name and the url to feetch it from.
    /// The world files have to be `.tar.gz` files.
    ///
    /// ## Example
    ///
    /// ```yaml
    /// repository: "https://github.com/SBPrime/"
    ///
    /// plugins:
    ///   - name: Vortex
    ///     url: "https://gitlab.com/api/v4/projects/52073948/packages/generic/v0.1.0/plugin/Vortex.jar"
    ///     version: 0.1.0
    ///   - name: AsyncWorldEdit
    ///     version: 3.9.4
    ///
    /// worlds:
    ///   - name: Main
    ///     url: "https://gitlab.com/e4333/plugins/Vortex/-/archive/v0.1.1/Vortex-v0.1.1.tar.gz"
    /// ```
    ///
    /// An other example of config file can be found in [test/example.yml](https://gitlab.com/e4333/docker-images/server-loader/-/blob/main/test/example.yml) in the [git repository](https://gitlab.com/e4333/docker-images/server-loader)
    ///
    pub fn new<T: Into<String> + AsRef<Path>>(filename: T) -> Self {
        let file = match File::open(&filename) {
            Ok(file) => file,
            Err(err) => match err.kind() {
                ErrorKind::NotFound => panic!("The file {} does not exist.", filename.into()),
                _ => unreachable!("Unexpected error : {:?}", err),
            }
        };
        serde_yaml::from_reader::<File, Self>(file).expect("The file provided should be a valid file.")
    }

    /// Loads (fetches) the plugins contained in the Config
    pub fn load_plugins<T: Into<String> + AsRef<Path> + Clone>(&self, folder: T, oauth_token: &Option<String>) {
        create_dir_all(&folder).expect("The plugins' folder could not be created.");

        if let Some(plugins) = &self.plugins {
            for plugin in plugins {
                plugin.load(folder.clone(), &self.repo, oauth_token).expect(&("Error during the load of ".to_owned() + plugin.get_name()))
            }
        }
    }


    /// Loads (fetches) the worlds contained in the Config
    pub fn load_worlds<T: Into<String> + AsRef<Path> + Clone>(&self, folder: T, oauth_token: &Option<String>) {
        create_dir_all(&folder).expect("The worlds' folder could not be created.");

        if let Some(worlds) = &self.worlds {
            for world in worlds {
                world.load(folder.clone(), oauth_token).expect(&("Error during the load of ".to_owned() + world.get_name()))
            }
        }
    }
}


#[cfg(test)]
mod test {

    use super::Config;

    use tempfile::TempDir;

    #[test]
    fn open_config() {
        assert_eq!(Config::new("test/input.yml"), Config{repo: None, plugins: None, worlds: None})
    }

    #[test]
    fn load_plugins() {
        let tmp_dir = TempDir::new().unwrap();

        Config::new("test/example.yml").load_plugins(tmp_dir.path().to_str().unwrap(), &None)
    }

    #[test]
    fn load_worlds() {
        let tmp_dir = TempDir::new().unwrap();

        Config::new("test/example.yml").load_worlds(tmp_dir.path().to_str().unwrap(), &None)
    }

    #[test]
    #[should_panic(expected = "The file test/non_existing.yml does not exist.")]
    fn open_non_existing() {
        Config::new("test/non_existing.yml");
    }
}
