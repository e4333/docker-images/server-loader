// This file is part of Server Loader.
//
// Server Loader is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Server Loader is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Server Loader. If not, see <https://www.gnu.org/licenses/>.


use std::io::Write;
use curl::easy::Easy;
use std::fs::File;

use crate::error::Error;


pub struct Url{
    url: String,
}

impl Url {
    /// Creates a new Url object
    ///
    /// # Example
    /// ```
    /// use server_loader::url::Url;
    ///
    /// Url::new("www.ultraxime.fr", &None);
    /// ```
    pub fn new<T: Into<String>>(url: T, oauth_token: &Option<String>) -> Self {
        let url: String = match oauth_token {
            None => url.into(),
            Some(token) => url.into().replace("${oauth_token}", token),
        };
        Self {
            url,
        }
    }


    /// Retrieve the target of the url in the given file
    ///
    /// # Example
    /// ```
    /// use server_loader::url::Url;
    /// use  tempfile::NamedTempFile;
    ///
    /// let file = NamedTempFile::new().unwrap().into_file();
    /// let url = Url::new("www.ultraxime.fr", &None);
    ///
    /// url.fetch_to_file(file).unwrap()
    /// ```
    pub fn fetch_to_file(&self, mut file: File) -> Result<(), Error> {
        let mut handle = Easy::new();

        handle.url(&self.url)?;

        handle.write_function(move |data| {
            Ok(file.write(data).unwrap())
        })?;

        Ok(handle.perform()?)
    }

    #[cfg(test)]
    fn get_url(&self) -> &String {
        &self.url
    }
}


#[cfg(test)]
mod test {

    use super::Url;

    use  tempfile::NamedTempFile;

    #[test]
    fn test_fetch_to_file() {
        let tmp = NamedTempFile::new().unwrap();
        assert_eq!(
            Url::new("www.ultraxime.fr", &None).fetch_to_file(tmp.into_file()).unwrap(),
            ());
    }

    #[test]
    fn test_replace(){
        assert_eq!(
            Url::new("www.ultraxime.fr", &None).get_url(),
            "www.ultraxime.fr");
        assert_eq!(
            Url::new("www.ultraxime.fr/${oauth_token}", &Some("test".to_string())).get_url(),
            "www.ultraxime.fr/test");
    }

}
