| ![gplv3-or-later](https://www.gnu.org/graphics/gplv3-or-later.png) | [![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit) |
|--------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------|

# Server Loader

## Description

A rust-based program used to fetch the plugins and worlds during the initialisation of a Minecraft Server

## Installation

To install it you can download the corresponding binary from the [releases](https://gitlab.com/e4333/docker-images/server-loader/-/releases)

### From Source

You can also install the software by :

- cloning the repository
- then using `cargo build --release`
- you will find the binary in `target/release`

## Usage

```
Usage: server-loader [OPTIONS] [INPUT]

Arguments:
  [INPUT]  Input file [default: server_loader.yml]

Options:
  -p, --plugins-only                     Plugins only
  -w, --worlds-only                      Servers only
      --plugins-folder <PLUGINS_FOLDER>  Plugins folder [default: plugin]
      --worlds-folder <WORLDS_FOLDER>    Worlds folder [default: .]
  -h, --help                             Print help
  -V, --version                          Print version
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

<!-- Please make sure to update tests as appropriate. -->

### Pre-Commit

The project already contains a pre-commit-config.

## Authors

- [@Ultraxime](https://gitlab.com/Ultraxime)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
